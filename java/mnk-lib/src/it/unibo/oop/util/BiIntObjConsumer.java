package it.unibo.oop.util;

@FunctionalInterface
public interface BiIntObjConsumer<E> {
    void apply(int i, int j, E e);
}
